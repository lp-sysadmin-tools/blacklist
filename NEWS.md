Release notes
-------------

### Version 7

Rename to banhost. It applies to the database tables too. There is no automatic update.

### Version 6

Support for nft.

The format of the configuration file has changed. Instead of

    [iptables]
    ...

It's

    [tables]
    framework: iptables

### Version 5

The database schema has changed. It must be migrated using action `--upgrade`. Backup the old database first.
