# Copyright (C) 2005-2019 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import argparse
import contextlib
import io
import json
import logging, logging.handlers
import os
import sys

from configparser import ConfigParser

from .version import __version__
from .core import *

### Functions

def init_logging(level=None, output=None, facility=None):
    """Initialize the logging system."""
    if output == 'syslog':
        if facility:
            if not facility in logging.handlers.SysLogHandler.facility_names:
                raise Exception('{}: invalid syslog facility'.format(facility))
            facility = logging.handlers.SysLogHandler.facility_names[facility]
        hdlr = logging.handlers.SysLogHandler('/dev/log', facility)
        fmt = 'banhost: %(levelname)s: %(message)s'
    elif output == None or output == 'stream':
        hdlr = logging.StreamHandler()
        fmt = '%(levelname)s: %(message)s'
    else:
        hdlr = logging.handlers.RotatingFileHandler(output)
        fmt = '%(asctime)s %(levelname)s: %(message)s'

    log_level = logging.WARNING
    if level == 'debug':
        log_level = logging.DEBUG
    elif level == 'verbose':
        log_level = logging.INFO
    elif level == 'quiet':
        log_level = logging.ERROR

    root = logging.getLogger('')
    root.setLevel(log_level)
    hdlr.setFormatter(logging.Formatter(fmt))
    root.addHandler(hdlr)

class JSONEncoder(json.JSONEncoder):

    def default(self, o):
        return o._asdict()

    @staticmethod
    def save(filename, obj):
        with io.open(filename, 'w') as outfh:
            json.dump(obj, outfh, cls=JSONEncoder)

### Main

class Printer:

    """Print the status in human readable format."""

    def __init__(self, output):
        self.output = output

    def print_hosts(self, title, host_list, with_direction=False):
        if host_list:
            write = self.output.write
            write('{}:\n'.format(title))
            for host in host_list:
                write(str(host.address))
                if with_direction and host.direction != host.DIRECTION_BOTH:
                    write(' ({})'.format('incoming' if host.direction == host.DIRECTION_INCOMING else 'outgoing'))
                write('\n')

    def print_status(self, status):
        """Print the status."""
        self.print_hosts("Banned hosts", status.banhost)
        self.print_hosts("Missing in netfilter", status.missing, with_direction=True)
        self.print_hosts("Remaining in netfilter", status.orphans, with_direction=True)


def run(args, config, output=sys.stdout):
    if args.database:
        config.set('files', 'database', args.database)
        logging.info("database: {}".format(args.database))
    if args.compact:
        config.set('files', 'compact_on_exit', 'yes')

    kw = { 'level': None,
           'output': None,
           'facility': None }
    if not args.stdout and config.has_section('logging'):
        for option_name in config.options('logging'):
            if not option_name in kw:
                raise Exception('{}: invalid option in section "logging"'.format(option_name))
            kw[option_name] = config.get('logging', option_name)
    if args.log_level:
        kw['level'] = args.log_level

    debug_mode = (kw['level'] == 'debug')

    init_logging(**kw)

    try:
        if args.upgrade:
            upgrade_database(config)
        else:
            with contextlib.closing(new_instance(config, args.no_act)) as banhost:
                if args.rebuild:
                    banhost.rebuild()
                elif args.update:
                    banhost.update()
                elif args.restore:
                    banhost.restore()
                elif args.remove:
                    for addr in args.ip_addresses:
                        banhost.remove(addr)
                elif args.ip_addresses:
                    for name_or_addr in args.ip_addresses:
                        banhost.add(name_or_addr)
                if args.json_file:
                    status = banhost.status()
                    JSONEncoder.save(args.json_file, status)
                elif args.html_file:
                    banhost.generate_html(args.html_file)
                elif args.list_hosts:
                    status = banhost.status()
                    Printer(output).print_status(status)
    except Exception as ex:
        logging.error(str(ex))
        raise

usage="""Ban remote hosts.

Configuration file syntax is:

  [files]
  database: /var/lib/banhost/banhost.db

  [tables]
  framework: nftables
  family: ip|ip6|inet
  table: <table>
  chain_in: <chain for incoming packets>
  target_in: <target to banhost incoming packets>
  chain_out: <chain for outgoing packets>
  target_out: <target to banhost outgoing packets>

  [policy]
  expire: <delay for banhosting a host in minutes>
  trusted: <space separated list of IP addresses>

  [history]
  expire: <delay before deleting an event in minutes>

  [logging]
  output: stdout|syslog
  facility: auth|daemon|user|...
  level: verbose|debug

Where:
  - delays are in minutes by default but can be suffixed by: h (hours), d (days), w(weeks)
"""


def main(executable=None, arguments=None):
    """Top level.

    For tests:
      EXECUTABLE is the path of the executable.
      ARGUMENTS must be the list of arguments after the command name as in sys.argv[1:]."""
    command = executable or os.path.abspath(sys.argv[0])
    program_name = os.path.splitext(os.path.basename(command))[0]
    install_prefix = os.path.dirname(os.path.dirname(command))

    # Command line

    argparser = argparse.ArgumentParser(prog=command, description=usage, formatter_class=argparse.RawDescriptionHelpFormatter)

    # Logging

    argparser.add_argument("--debug",
                           dest="log_level", action="store_const", const='debug',
                           help="debug mode, for developers only")

    argparser.add_argument("-v", "--verbose",
                           dest="log_level", action="store_const", const='verbose',
                           help="verbose mode")

    argparser.add_argument("-q", "--quiet",
                           dest="log_level", action="store_const", const='quiet',
                           help="quiet mode")

    argparser.add_argument("-S", "--stdout",
                           dest="stdout", action="store_true", default=False,
                           help="force logging on stdout.")

    # Actions

    argparser.add_argument("-K", "--compact",
                           dest="compact", action="store_true", default=False,
                           help="compact the database.")

    argparser.add_argument("--rebuild",
                           dest="rebuild", action="store_true", default=False,
                           help="rebuild the database from current tables rules.")

    argparser.add_argument("-r", "--remove",
                           dest="remove", action="store_true", default=False,
                           help="remove IP from banhost.")

    argparser.add_argument("-R", "--restore",
                           dest="restore", action="store_true", default=False,
                           help="restore the tables rules according to the database.")

    argparser.add_argument("-u", "--update",
                           dest="update", action="store_true", default=False,
                           help="update blocked hosts.")

    argparser.add_argument("--upgrade",
                           dest="upgrade", action="store_true", default=False,
                           help="upgrade database schema.")

    # Others

    argparser.add_argument("-c", "--config-file",
                           dest="config_file", action="store", default=None,
                           metavar="FILE",
                           help="configuration file.")

    argparser.add_argument("--database",
                           dest="database", action="store", default=None,
                           metavar="DATABASE",
                           help="override the database parameter in the configuration file.")

    argparser.add_argument("--json",
                           dest="json_file", action="store", default=None,
                           metavar="FILE",
                           help="save status in FILE in JSON format.")

    argparser.add_argument("--html",
                           dest="html_file", action="store", default=None,
                           metavar="FILE",
                           help="generate a HTML page based on the current status.")

    argparser.add_argument("--list",
                           dest="list_hosts", action="store_true",
                           help="print the list of blocked hosts.")

    argparser.add_argument("-n", "--no-act", "--dry-run",
                           dest="no_act", action="store_true", default=False,
                           help="dry run.")

    argparser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))

    argparser.add_argument("ip_addresses", nargs='*', metavar="IP")

    args = argparser.parse_args(args=arguments)

    # Configuration

    config = ConfigParser()

    config.add_section('tables')
    config.set('tables', 'framework', 'nftables')
    config.add_section('policy')
    config.set('policy', 'expire', '20') # minutes

    try:
        config_file = args.config_file
        if not config_file:
            default_config_name = '{}.conf'.format(program_name)
            for filename in [ os.path.join(install_prefix, 'etc', default_config_name), '/etc/{}'.format(default_config_name) ]:
                if os.path.isfile(filename):
                    config_file = filename
        if not config_file:
            raise Exception('no configuration file found')
        config.read(config_file)
        run(args, config)
    except:
        if args.log_level == 'debug':
            import traceback
            traceback.print_exc()
        else:
            sys.stderr.write('{}\n'.format(sys.exc_info()[1]))
        sys.exit(1)
