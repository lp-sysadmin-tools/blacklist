# Copyright (C) 2005-2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import contextlib
import io
import json
import os
import shutil
import tempfile
import unittest

from unittest.mock import call, patch

from collections import namedtuple

from tests.helpers.asserts import *
from tests.helpers.dns import *
from tests.helpers.subproc import FakePopen

import banhost
from banhost.core import BlockList, parse_delay
from banhost.network import Host, Event

CHAIN_IN = "in_banhost"
TARGET_IN = "BLACKLIST_INCOMING"
CHAIN_OUT = "out_banhost"
TARGET_OUT = "BLACKLIST_OUTGOING"

QUARANTINE = 2 * 3600
HISTORY_DELAY = 7 * 24 * 3600

import html.parser
from html.parser import HTMLParser

class HTMLParser(html.parser.HTMLParser):

    """HTML parser that collects tags."""

    def __init__(self):
        super().__init__()
        self.tags = {}

    def handle_starttag(self, tag, attrs):
        self.tags.setdefault(tag, 0)
        self.tags[tag] += 1

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        pass

class TestFunctions(unittest.TestCase):

    """Test static functions."""

    def test_parse_delay(self):
        """Test function parse_delay"""
        hour_in_seconds = 3600
        day_in_seconds = 24 * hour_in_seconds
        for delay, seconds in [ (None, 0), ('17s', 17), ('3m', 3 * 60), ('3', 3 * 60),
                                ('6h', 6 * hour_in_seconds), ('2d', 2 * day_in_seconds), ('1w', 7 * day_in_seconds) ]:
            self.assertEqual(parse_delay(delay), seconds)

    def test_parse_delay_errors(self):
        """Errors in function parse_delay"""
        for delay in [ 's', '18k' ]:
            with self.assertRaises(Exception):
                parse_delay(delay)

class FakeIpTablesPopenBuilder:

    # iptables -n -L out_banhost
    # Chain out_banhost (1 references)
    # target     prot opt source               destination
    # BLACKLIST_OUTGOING  all  --  0.0.0.0/0            183.3.202.200
    # BLACKLIST_OUTGOING  all  --  0.0.0.0/0            193.201.225.26
    # BLACKLIST_OUTGOING  all  --  0.0.0.0/0            200.26.138.180
    # BLACKLIST_OUTGOING  all  --  0.0.0.0/0            202.99.172.155
    # iptables -n -L in_banhost
    # Chain in_banhost (1 references)
    # target     prot opt source               destination
    # BLACKLIST_INCOMING  all  --  183.3.202.200        0.0.0.0/0
    # BLACKLIST_INCOMING  all  --  193.201.225.26       0.0.0.0/0
    # BLACKLIST_INCOMING  all  --  200.26.138.180       0.0.0.0/0
    # BLACKLIST_INCOMING  all  --  202.99.172.155       0.0.0.0/0

    HEADER_1 = "Chain {chain} (1 references)"
    HEADER_2 = "target     prot opt source               destination"
    LINE_IN = "{target}  all  --  {address}        0.0.0.0/0"
    LINE_OUT = "{target}  all  --  0.0.0.0/0            {address}"

    def __init__(self):
        self.hosts = {}
        self.stdout = []

    def wait(self, **kw):
        return 0

    def add_hosts(self, hosts):
        for host in hosts:
            hosts_per_version = self.hosts.setdefault(host.ipversion, [])
            hosts_per_version.append(host)

    def write(self, line):
        self.stdout.append('{}\n'.format(line).encode('UTF-8'))

    def __call__(self, args, **kw):
        command = os.path.basename(args[0])
        if command == 'ip4tables':
            ipversion = 4
        elif command == 'ip6tables':
            ipversion = 6
        else:
            raise Exception('invalid mock for arguments: {}'.format(args))
        chain = args[-1]
        self.write(self.HEADER_1.format(chain=chain))
        self.write(self.HEADER_2)
        if chain == CHAIN_IN:
            target = TARGET_IN
            line = self.LINE_IN
        elif chain == CHAIN_OUT:
            target = TARGET_OUT
            line = self.LINE_OUT
        if ipversion in self.hosts:
            for host in self.hosts[ipversion]:
                self.write(line.format(target=target, address=host.address))
        return self

class TestBlocklistIpTables(unittest.TestCase):

    """Test class Blocklist with iptables framework."""

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_banhost')

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def new_instance(self):
        iptables = banhost.netfilter.IpTables(CHAIN_IN, TARGET_IN, CHAIN_OUT, TARGET_OUT, False)
        iptables.set_command([ '/disable/ip4tables' ], ipversion=4)
        iptables.set_command([ '/disable/ip6tables' ], ipversion=6)
        dbname = os.path.join(self.working_dir, 'banhost.db')
        return BlockList(dbname, iptables, QUARANTINE, HISTORY_DELAY)

    def expected_calls(self, action, *addresses, previous_calls=None):
        """Return the list of expected calls for append, delete or list."""
        calls = previous_calls or []
        if action == 'list':
            for chain in [CHAIN_IN, CHAIN_OUT]:
                for ip_version in [4, 6]:
                    command = '/disable/ip{}tables'.format(ip_version)
                    calls.append(call([command, '--numeric', '--list', chain]))
        elif action in ('append', 'delete'):
            check = (action == 'append')
            option = '--{}'.format(action)
            for addr in addresses:
                command = '/disable/ip{}tables'.format(Host.guess_ip_version(addr))
                calls.append(call([command, option, CHAIN_IN, '--source', addr, '--jump', TARGET_IN], check=check))
                calls.append(call([command, option, CHAIN_OUT, '--destination', addr, '--jump', TARGET_OUT], check=check))
        else:
            self.fail('{}: invalid action'.format(action))
        return calls

    def mocked_status(self, instance):
        """Call status with a mocked process call."""
        with patch('banhost.netfilter.Capture.__call__', return_value=0) as mock_capture:
            with patch('logging.error'): # mocked iptables doesn't list rules
                status = instance.status()
            mock_capture.assert_has_calls(self.expected_calls('list'))
            return status

    def mocked_add_hosts(self, instance, hosts):
        """Call add_host with a mocked process call."""
        expected_calls = self.expected_calls('append', *tuple([ host.address for host in hosts ]))
        with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
            for host in hosts:
                instance.add_host(host)
            mock_call.assert_has_calls(expected_calls)

    def mocked_add_addresses(self, instance, addresses):
        """Call add_host resolving addresses with a mocked process call."""
        hosts = []
        for addr in addresses:
            for host in Host.resolve_hosts(addr):
                hosts.append(host)
        self.mocked_add_hosts(instance, hosts)

    def mocked_update(self, instance, hosts):
        """Call update and expects hosts to be removed."""
        expected_calls = self.expected_calls('delete', *tuple([ host.address for host in hosts ]))
        with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
            instance.update()
            mock_call.assert_has_calls(expected_calls)

    @patch('socket.gethostname', return_value='localhost')
    def test_no_rules(self, mock_gethostname):
        """No rules"""
        with contextlib.closing(self.new_instance()) as instance:
            status = self.mocked_status(instance)
            for name in ['banhost', 'missing', 'orphans', 'history']:
                self.assertTrue(hasattr(status, name), "{}: not an attribute of status".format(name))
            self.assertEqual([], status.banhost)
            self.assertEqual([], status.missing)
            self.assertEqual([], status.orphans)
            self.assertEqual([], status.history)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_add_one_rule_by_IP_address(self, mock_gethostname, mock_getaddrinfo):
        """Add rules by IP address"""
        in_addresses = sorted([ TEST_IPV4_ADDRESS1, TEST_IPV6_ADDRESS1 ])
        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_addresses(instance, in_addresses)
            status = self.mocked_status(instance)
            out_addresses = sorted([ host.address for host in status.banhost ])
            self.assertEqual(in_addresses, out_addresses)
        assert_called_once(mock_gethostname)
        assert_called(mock_getaddrinfo)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_add_one_rule_by_name(self, mock_gethostname, mock_getaddrinfo):
        """Add rules by name"""
        in_addresses = sorted([ TEST_IPV4_ADDRESS1, TEST_IPV6_ADDRESS1 ])
        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_hosts(instance, Host.resolve_hosts(TEST_HOST_NAME1))
            status = self.mocked_status(instance)
            out_addresses = sorted([ host.address for host in status.banhost ])
            self.assertEqual(in_addresses, out_addresses)
        assert_called_once(mock_gethostname)
        assert_called(mock_getaddrinfo)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_add_one_rule_by_name_or_address(self, mock_gethostname, mock_getaddrinfo):
        """Add rules by name or address"""
        with contextlib.closing(self.new_instance()) as instance:
            expected_calls = self.expected_calls('append', TEST_IPV6_ADDRESS1, TEST_IPV4_ADDRESS1)
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                instance.add(TEST_HOST_NAME1)
                mock_call.assert_has_calls(expected_calls)
        assert_called_once(mock_gethostname)
        assert_called(mock_getaddrinfo)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_add_rule_twice(self, mock_gethostname, mock_getaddrinfo):
        """Add rule twice"""
        in_addresses = sorted([ TEST_IPV6_ADDRESS1 ])
        hosts = Host.resolve_hosts(in_addresses[0])
        now = Event.now()
        for timestamp in [ now - QUARANTINE, now ]:
            with contextlib.closing(self.new_instance()) as instance:
                self.mocked_add_hosts(instance, hosts)
        with contextlib.closing(self.new_instance()) as instance:
            status = self.mocked_status(instance)
            out_addresses = sorted([ host.address for host in status.banhost ])
            self.assertEqual(in_addresses, out_addresses)
        assert_called(mock_gethostname)
        assert_called(mock_getaddrinfo)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_remove_one_rule(self, mock_gethostname, mock_getaddrinfo):
        """Remove one rule"""
        in_addresses = sorted([ TEST_IPV4_ADDRESS1, TEST_IPV6_ADDRESS1 ])
        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_addresses(instance, in_addresses)

        with contextlib.closing(self.new_instance()) as instance:
            expected_calls = self.expected_calls('delete', in_addresses[0])
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                instance.remove(in_addresses[0])
                mock_call.assert_has_calls(expected_calls)

        assert_called(mock_getaddrinfo)
        assert_called(mock_gethostname)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_host_not_in_database(self, mock_gethostname, mock_getaddrinfo):
        """Blocked host not in database."""
        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_addresses(instance, [TEST_IPV4_ADDRESS1])
            instance.database.clear_current_events()
        with contextlib.closing(self.new_instance()) as instance:
            status = self.mocked_status(instance)
            self.assertEqual(status.banhost, [])

        assert_called(mock_gethostname)
        assert_called(mock_getaddrinfo)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_host_not_blocked(self, mock_gethostname, mock_getaddrinfo):
        """Host in database but not blocked."""
        hosts = Host.resolve_hosts(TEST_IPV4_ADDRESS1)
        with contextlib.closing(self.new_instance()) as instance:
            for host in hosts:
                event = Event.make(host, Event.now() - QUARANTINE * 2, duration=QUARANTINE)
                instance.database.insert_new_event(event)
        with contextlib.closing(self.new_instance()) as instance:
            status = self.mocked_status(instance)
            self.assertEqual([ TEST_IPV4_ADDRESS1 ], [ host.address for host in status.banhost ])
            expected_calls = self.expected_calls('delete', *tuple([ host.address for host in hosts ]))[0:1]
            with patch('banhost.netfilter.Call.__call__', return_value=1) as mock_call:
                with patch('logging.error'):
                    instance.update()
                mock_call.assert_has_calls(expected_calls)

        assert_called(mock_gethostname)
        assert_called(mock_getaddrinfo)

    @patch('socket.gethostname', return_value='localhost')
    def test_multiple_occurrences(self, mock_gethostname):
        """Host blocked multiple times."""
        now = Event.now()
        host = Host(TEST_IPV6_ADDRESS1, 6)

        with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
            expected_calls = self.expected_calls('append', host.address)
            with contextlib.closing(self.new_instance()) as instance:
                instance.add_host(host, now - QUARANTINE * 2)
            mock_call.assert_has_calls(expected_calls)

        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_update(instance, [ host ])
            self.mocked_add_hosts(instance, [ host ])
            status = self.mocked_status(instance)
            self.assertEqual([ host.address for host in status.banhost ], [ TEST_IPV6_ADDRESS1 ])
            self.assertEqual([ host.address for host in status.history ], [ TEST_IPV6_ADDRESS1 ])
            only_one = (len(status.banhost) == 1 and len(status.history) == 1)
            self.assertTrue(only_one)
            if only_one:
                old_host = status.history[0]
                current_host = status.banhost[0]
                self.assertEqual(old_host.address, current_host.address)
                old_quarantine = old_host.end - old_host.start
                current_quarantine = current_host.end - current_host.start
                self.assertTrue(current_quarantine > old_quarantine + old_quarantine/2, "quarantine hasn't increased")

        assert_called(mock_gethostname)

    @patch('socket.gethostname', return_value='localhost')
    def test_host_added_twice(self, mock_gethostname):
        """Host blocked twice in a row."""
        now = Event.now()
        host = Host(TEST_IPV6_ADDRESS1, 6)

        with contextlib.closing(self.new_instance()) as instance:
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                expected_calls = self.expected_calls('append', host.address)
                instance.add_host(host, now - QUARANTINE)
                mock_call.assert_has_calls(expected_calls)
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                with patch('logging.warning'):
                    instance.add_host(host)
                assert_not_called(mock_call)

    @patch('socket.gethostname', return_value='localhost')
    def test_history_update(self, mock_gethostname):
        """History update after quarantine"""
        now = Event.now()
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4)

        expected_calls = self.expected_calls('append', host1.address, host2.address)
        with contextlib.closing(self.new_instance()) as instance:
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                instance.add_host(host1, now - QUARANTINE * 2)
                instance.add_host(host2)
                mock_call.assert_has_calls(expected_calls)

        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_update(instance, [ host1 ])
            status = self.mocked_status(instance)
            current_addresses = [ host.address for host in status.banhost ]
            self.assertEqual([ host2.address ], current_addresses)
            old_addresses = [ host.address for host in status.history ]
            self.assertEqual([ host1.address ], old_addresses)

        assert_called(mock_gethostname)

    @patch('socket.gethostname', return_value='localhost')
    def test_history_cleanup(self, mock_gethostname):
        """History cleanup"""
        now = Event.now()
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4)

        expected_calls = self.expected_calls('append', host1.address, host2.address)
        with contextlib.closing(self.new_instance()) as instance:
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                instance.add_host(host1, now - HISTORY_DELAY * 2)
                instance.add_host(host2, now - QUARANTINE * 2)
                mock_call.assert_has_calls(expected_calls)
            self.mocked_update(instance, [ host1, host2 ])
            status = self.mocked_status(instance)
            self.assertEqual(status.banhost, [])
            in_addresses = sorted([ host.address for host in [ host1, host2 ] ])
            out_addresses = sorted([ host.address for host in status.history ])
            self.assertEqual(in_addresses, out_addresses)

        # Run update on empty database
        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_update(instance, [])
            status = self.mocked_status(instance)
            self.assertEqual(status.banhost, [])
            in_addresses = [ host2.address ]
            out_addresses = sorted([ host.address for host in status.history ])
            self.assertEqual(in_addresses, out_addresses, "history is {}".format(status.history))

        assert_called(mock_gethostname)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_rebuild_database(self, mock_gethostname, mock_getaddrinfo):
        """Rebuild the database."""
        fake_popen = FakeIpTablesPopenBuilder()
        past_start_time = Event.now() - QUARANTINE * 2
        deleted_event = None
        remaining_events = {}
        with contextlib.closing(self.new_instance()) as instance:
            dbfile = instance.database.connection.filename
            hosts = Host.resolve_hosts(TEST_HOST_NAME1)
            fake_popen.add_hosts(hosts)
            self.mocked_add_hosts(instance, hosts)
            for event in instance.database.iterate_current_events():
                if not deleted_event:
                    deleted_event = event
                    instance.database.delete_current_event(event.host.address)
                else:
                    remaining_events[event.host.address] = event

        with contextlib.closing(self.new_instance()) as instance:
            status = self.mocked_status(instance)
            out_addresses = sorted([ host.address for host in status.banhost ])
            remaining_addresses = sorted([ address for address in remaining_events ])
            self.assertEqual(remaining_addresses, out_addresses)
            with patch('subprocess.Popen', return_value=0) as mock_popen:
                mock_popen.side_effect = fake_popen
                instance.rebuild()
            status = self.mocked_status(instance)
            in_addresses = sorted([ TEST_IPV4_ADDRESS1, TEST_IPV6_ADDRESS1 ])
            out_addresses = sorted([ host.address for host in status.banhost ])
            self.assertEqual(in_addresses, out_addresses)
            kept = rebuilt = 0
            for event in instance.database.iterate_current_events():
                address = event.host.address
                if address in remaining_events:
                    self.assertEqual(event.start_time, remaining_events[address].start_time, "event start time not preserved")
                    kept += 1
                else:
                    self.assertTrue(event.start_time > past_start_time)
                    rebuilt += 1
            self.assertTrue(kept > 0, "no event has been preserved")
            self.assertTrue(rebuilt > 0, "no event has been rebuilt")

        assert_called(mock_gethostname)
        assert_called(mock_getaddrinfo)

    @patch('logging.error')
    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_restore_rules(self, mock_gethostname, mock_getaddrinfo, mock_log_error):
        """Restore rules."""
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4)
        host3 = Host(TEST_IPV4_ADDRESS2, 4)
        fake_popen = FakeIpTablesPopenBuilder()
        fake_popen.add_hosts([ host2, host3 ])
        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_hosts(instance, [host1, host2])
            expected_calls = self.expected_calls('append', host1.address)
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                with patch('subprocess.Popen', return_value=0) as mock_popen:
                    mock_popen.side_effect = fake_popen
                    instance.restore()
                    mock_call.assert_has_calls(expected_calls)
        self.assertEqual(6, mock_log_error.call_count)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_report_error_on_orphaned_rules(self, mock_gethostname, mock_getaddrinfo):
        """Report an error for blocked hosts not in database."""
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        fake_popen = FakeIpTablesPopenBuilder()
        fake_popen.add_hosts([ host1 ])
        message_fmt = '{}: blocked host in chain {} not in database'
        expected_messages = [ message_fmt.format(host1.address, CHAIN_IN),
                              message_fmt.format(host1.address, CHAIN_OUT) ]
        with contextlib.closing(self.new_instance()) as instance:
            with patch('subprocess.Popen', return_value=0) as mock_popen:
                mock_popen.side_effect = fake_popen
                with patch('logging.error') as mock_log_error:
                    status = instance.status()
                    mock_log_error.assert_has_calls([call(msg) for msg in expected_messages])

    @patch('socket.gethostname', return_value='localhost')
    def test_generate_HTML(self, mock_gethostname):
        """Generate HTML file"""
        now = Event.now()
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4)
        host3 = Host(TEST_IPV6_ADDRESS3, 6)

        with contextlib.closing(self.new_instance()) as instance:
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                expected_calls = self.expected_calls('append', host2.address)
                instance.add_host(host2, now - QUARANTINE * 4)
                mock_call.assert_has_calls(expected_calls)
            self.mocked_update(instance, [ host2 ])
            with patch('banhost.netfilter.Call.__call__', return_value=0) as mock_call:
                expected_calls = self.expected_calls('append', host1.address, host2.address)
                instance.add_host(host1, now - HISTORY_DELAY * 2)
                instance.add_host(host2, now - QUARANTINE * 2)
                mock_call.assert_has_calls(expected_calls)
            self.mocked_update(instance, [ host2 ])
            self.mocked_add_hosts(instance, [ host3 ])

        html_file = os.path.join(self.working_dir, 'index.html')
        with contextlib.closing(self.new_instance()) as instance:
            self.assertNotEqual(0, len(instance.database.collect_current_events()))
            self.assertNotEqual(0, len(instance.database.collect_past_events()))
            instance.generate_html(html_file)
        file_exists = os.path.isfile(html_file)
        self.assertTrue(file_exists, "{}: doesn't exist".format(html_file))
        if file_exists:
            parser = HTMLParser()
            with io.open(html_file) as infh:
                parser.feed(infh.read())
            self.assertIn('canvas', parser.tags)
            if 'canvas' in parser.tags:
                self.assertEqual(1, parser.tags['canvas'])
            self.assertIn('table', parser.tags)
            if 'table' in parser.tags:
                self.assertEqual(2, parser.tags['table'])

        assert_called(mock_gethostname)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_trusted(self, mock_gethostname, mock_getaddrinfo):
        """No banhost host in trusted."""
        with contextlib.closing(self.new_instance()) as instance:
            instance.add_to_trusted(TEST_IPV4_ADDRESS1)
            instance.add_host(Host(TEST_IPV4_ADDRESS1, 4))
        with contextlib.closing(self.new_instance()) as instance:
            status = self.mocked_status(instance)
            self.assertEqual([], status.banhost)
            self.assertEqual([], status.history)
        assert_called(mock_gethostname)
        assert_called(mock_getaddrinfo)


NFT_LIST_IN = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "in_banhost", "handle": 25}}, {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "handle": 35, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip", "field": "saddr"}}, "right": "__IPV4__"}}, {"jump": {"target": "pr_in_banhost"}}]}}, {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "handle": 33, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "saddr"}}, "right": "__IPV6__"}}, {"jump": {"target": "pr_in_banhost"}}]}}]}
"""

NFT_LIST_OUT = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "out_banhost", "handle": 26}}, {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "handle": 36, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip", "field": "daddr"}}, "right": "__IPV4__"}}, {"jump": {"target": "pr_out_banhost"}}]}}, {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "handle": 34, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "daddr"}}, "right": "__IPV6__"}}, {"jump": {"target": "pr_out_banhost"}}]}}]}
"""

class TestBlocklistNfTables(unittest.TestCase):

    """Test class Blocklist with nftables framework."""

    FAMILY = 'inet'
    TABLE = 'filter'

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_banhost')
        for name, fmt in [ ('list_of_in_rules', NFT_LIST_IN), ('list_of_out_rules', NFT_LIST_OUT) ]:
            setattr(self, name,
                    fmt
                    .replace('__IPV4__', TEST_IPV4_ADDRESS1)
                    .replace('__IPV6__', TEST_IPV6_ADDRESS2))
        self.list_of_rules = self.list_of_in_rules + self.list_of_out_rules

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def new_instance(self):
        nftables = banhost.netfilter.NfTables(self.FAMILY, self.TABLE, CHAIN_IN, TARGET_IN, CHAIN_OUT, TARGET_OUT, False)
        nftables.set_command([ '/disable/nft' ])
        dbname = os.path.join(self.working_dir, 'banhost.db')
        return BlockList(dbname, nftables, QUARANTINE, HISTORY_DELAY)

    def mocked_add_hosts(self, instance, hosts):
        """Call add_host with a mocked process call.

        A host in the list can be a pair (host, start_time)."""
        with patch('banhost.process.JsonProcess.communicate', return_value='') as mock_communicate:
            for host in hosts:
                start_time = None
                if not isinstance(host, Host):
                    host, start_time = host
                instance.add_host(host, start_time)
            self.assertEqual(len(hosts), mock_communicate.call_count)

    @patch('logging.error')
    @patch('socket.gethostname', return_value='localhost')
    def test_history_update_with_errors(self, mock_gethostname, mock_log_error):
        """History update after quarantine with error"""
        now = Event.now()
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4)

        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_hosts(instance, [ (host1, now - QUARANTINE * 2), host2 ])

        with contextlib.closing(self.new_instance()) as instance:
            side_effect = [ FakePopen(self.list_of_rules, 0), # list handles
                            FakePopen('', 1) ]                # delete with exit code 1
            with patch('subprocess.Popen', side_effect=side_effect) as mock_popen:
                instance.update()
                self.assertEqual(2, mock_popen.call_count)

        assert_called_once(mock_log_error)
        assert_called(mock_gethostname)

    @patch('logging.error')
    @patch('socket.gethostname', return_value='localhost')
    def test_status_with_discrepancies(self, mock_gethostname, mock_log_error):
        """Status with discrepancies between the database and the netfilter rules

        Missing: hosts are in the database but not in netfilter rules.
        Orphans: hosts are in the rules but not in the database.
        """
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4) # not in database
        host3 = Host(TEST_IPV4_ADDRESS2, 4) # not in rules

        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_hosts(instance, [ host1, host3 ])

        with contextlib.closing(self.new_instance()) as instance:
            side_effect = [ FakePopen(self.list_of_in_rules, 0), FakePopen(self.list_of_out_rules, 0) ]
            with patch('subprocess.Popen', side_effect=side_effect) as mock_popen:
                status = instance.dump_banhost()
                self.assertEqual([ host3 ], status.missing)
                self.assertEqual([ host2 ], status.orphans)

        self.assertEqual(4, mock_log_error.call_count)

    def mocked_restore(self, in_rules, out_rules):
        with contextlib.closing(self.new_instance()) as instance:
            side_effect = [ FakePopen(in_rules, 0), FakePopen(out_rules, 0) ]
            with patch('subprocess.Popen', side_effect=side_effect) as mock_popen:
                with patch('banhost.netfilter.NfTables.bulk_append') as mock_append:
                    with patch('banhost.netfilter.NfTables.bulk_delete') as mock_delete:
                        instance.restore()
                        return mock_append, mock_delete

    @patch('logging.error')
    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_restore_incoming_rules(self, mock_gethostname, mock_getaddrinfo, mock_log_error):
        """Restore rules."""
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4)
        host3 = Host(TEST_IPV6_ADDRESS1, 6)

        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_hosts(instance, [host1, host2])

        in_rules = json.loads(self.list_of_in_rules)
        in_rules['nftables'][-1]['rule']['expr'][0]['match']['right'] = host3.address
        in_rules = json.dumps(in_rules)
        mock_append, mock_delete = self.mocked_restore(in_rules, self.list_of_out_rules)
        mock_append.assert_called_once_with([host1])
        self.assertEqual(Host.DIRECTION_INCOMING, mock_append.call_args[0][0][0].direction)
        mock_delete.assert_called_once_with([host3])
        self.assertEqual(Host.DIRECTION_INCOMING, mock_delete.call_args[0][0][0].direction)
        self.assertEqual(2, mock_log_error.call_count)
        assert_called(mock_getaddrinfo)
        assert_called(mock_gethostname)

    @patch('logging.error')
    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    def test_restore_outgoing_rules(self, mock_gethostname, mock_getaddrinfo, mock_log_error):
        """Restore rules."""
        host1 = Host(TEST_IPV6_ADDRESS2, 6)
        host2 = Host(TEST_IPV4_ADDRESS1, 4)
        host3 = Host(TEST_IPV6_ADDRESS1, 6)

        with contextlib.closing(self.new_instance()) as instance:
            self.mocked_add_hosts(instance, [host1, host2])

        out_rules = json.loads(self.list_of_out_rules)
        out_rules['nftables'][-1]['rule']['expr'][0]['match']['right'] = host3.address
        out_rules = json.dumps(out_rules)
        mock_append, mock_delete = self.mocked_restore(self.list_of_in_rules, out_rules)
        mock_append.assert_called_once_with([host1])
        self.assertEqual(Host.DIRECTION_OUTGOING, mock_append.call_args[0][0][0].direction)
        mock_delete.assert_called_once_with([host3])
        self.assertEqual(Host.DIRECTION_OUTGOING, mock_delete.call_args[0][0][0].direction)
        self.assertEqual(2, mock_log_error.call_count)
        assert_called(mock_getaddrinfo)
        assert_called(mock_gethostname)

if __name__ == "__main__":
    unittest.main()
