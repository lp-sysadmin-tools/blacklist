# Copyright (C) 2005-2016 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import contextlib
import os
import shutil
import tempfile
import unittest

from unittest.mock import patch
from tests.helpers.dns import *
from tests.helpers.data import *

import banhost
from banhost.sqlite import Connection, Database, DatabaseError, DatabaseUpgrade, Transaction

def table_info2tuple(connection, table):
    result = connection.table_info(table)
    if not result:
        return None
    return [ (c.name, c.type) for c in result ]

class TestHostTable(unittest.TestCase):

    def test_make(self):
        """Make a host object"""
        self.assertIsNone(banhost.sqlite.HostTable.make(None))
        expected_host = banhost.network.Host(TEST_IPV6_ADDRESS1, 6, fake_gethostname(TEST_IPV6_ADDRESS1))
        host = banhost.sqlite.HostTable.make([expected_host.address, expected_host.ipversion, expected_host.name])
        self.assertEqual(expected_host, host)

class TestEventTable(unittest.TestCase):

    def test_make(self):
        """Make an event object"""
        self.assertIsNone(banhost.sqlite.EventTable.make(None))
        host = banhost.network.Host(TEST_IPV6_ADDRESS1, 6, fake_gethostname(TEST_IPV6_ADDRESS1))
        expected_event = banhost.network.Event.make(host, duration=30)
        event = banhost.sqlite.EventTable.make([host.address, host.ipversion, host.name,
                                                  expected_event.start_time, expected_event.end_time])
        self.assertEqual(expected_event, event)

class TestTransaction(unittest.TestCase):

    TABLE = banhost.sql.Table('persons', [ ('id', 'integer'), ('name', 'text'), ('age', 'integer') ])

    SCHEMA = "CREATE TABLE {} (id integer PRIMARY KEY, name text, age integer)".format(TABLE.name)

    INSERTIONS = [ "INSERT INTO persons VALUES (NULL, 'bob', 25)",
                   "INSERT INTO persons VALUES (NULL, 'alice', 38)",
                   "INSERT INTO persons VALUES (NULL, 'daisy', 18)" ]

    @classmethod
    def populate(self, connection):
        with connection.transaction() as t1:
            t1.execute((self.SCHEMA,))
        with connection.transaction() as t2:
            for stmt in self.INSERTIONS:
                t2.execute((stmt,))

    def test_fetch_one(self):
        """Fetch one row."""
        with Connection(':memory:') as connection:
            self.populate(connection)
            with connection.transaction() as t1:
                row = t1.fetch_one(('SELECT name, age FROM persons WHERE name = ?', ('alice',)))
                self.assertIsNotNone(row)
                self.assertEqual(('alice', 38), row)
            with connection.transaction() as t2:
                with self.assertRaises(DatabaseError):
                    t2.fetch_one(('SELECT name, age FROM persons WHERE age > ?', (18,)))

    def test_fetch_one_value(self):
        """Fetch one single value."""
        with Connection(':memory:') as connection:
            self.populate(connection)
            with connection.transaction() as t1:
                age = t1.fetch_single(('SELECT age FROM persons WHERE name = ?', ('alice',)))
                self.assertIsNotNone(age)
                self.assertEqual(38, age)
            with connection.transaction() as t2:
                with self.assertRaises(DatabaseError):
                    t2.fetch_single(('SELECT age FROM persons WHERE age > ?', (18,)))


class TestConnection(unittest.TestCase):

    def test_table_exists(self):
        """Test if a table exists."""
        with Connection(':memory:') as connection:
            TestTransaction.populate(connection)
            unknown_table = banhost.sql.Table('unknown', [ ('id', 'integer') ])
            self.assertFalse(connection.table_exists(unknown_table))
            self.assertTrue(connection.table_exists(TestTransaction.TABLE))

    def test_table_info(self):
        """Test table info."""
        with Connection(':memory:') as connection:
            TestTransaction.populate(connection)
            result = table_info2tuple(connection, TestTransaction.TABLE)
            self.assertEqual(TestTransaction.TABLE.columns, result)

class TestDatabase(unittest.TestCase):

    """Test class Database."""

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_banhost')
        self.dbname = os.path.join(self.working_dir, 'banhost.db')

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def test_creation(self):
        """Test database creation."""
        self.assertFalse(os.path.exists(self.dbname))
        with contextlib.closing(Database(self.dbname)) as db:
            self.assertTrue(os.path.exists(self.dbname))
            for table in db.TABLES:
                self.assertTrue(db.connection.table_exists(table))

    def test_creation_failure(self):
        """Test database creation failure."""
        with patch.object(banhost.sql.Table, 'schema') as mock_schema:
            mock_schema.return_value = 'CREATE TABLE invalid name (id integer)'
            with self.assertRaises(DatabaseError):
                with contextlib.closing(Database(self.dbname)) as db:
                    pass

    @patch('banhost.network.gethostname', return_value=True)
    def test_get_host(self, mock_gethostname):
        """Test if hosts are inserted."""
        self.assertFalse(os.path.exists(self.dbname))
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        event1 = banhost.network.Event.make(host1, duration=10)

        # Add event
        with contextlib.closing(Database(self.dbname)) as db:
            self.assertEqual(0, len(db.collect_current_events()))
            self.assertIsNone(event1.host.name)
            db.insert_new_event(event1)
            self.assertIsNotNone(event1.host.name)

        # Get host
        with contextlib.closing(Database(self.dbname)) as db:
            host = db.get_host(host1.address)
            self.assertEqual(host1, host)

        # Get unknown host
        with contextlib.closing(Database(self.dbname)) as db:
            host = db.get_host(TEST_IPV6_ADDRESS3)
            self.assertIsNone(host)

    @patch('banhost.network.gethostname', return_value=True)
    def test_get_event(self, mock_gethostname):
        """Get events."""
        self.assertFalse(os.path.exists(self.dbname))
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        event1 = banhost.network.Event.make(host1, duration=10)

        # Add event
        with contextlib.closing(Database(self.dbname)) as db:
            db.insert_new_event(event1)

        # Get event
        with contextlib.closing(Database(self.dbname)) as db:
            event = db.get_current_event_for(host1.address)
            self.assertIsNotNone(event)
            self.assertEqual(event1, event)
            event = db.get_current_event_for(TEST_IPV6_ADDRESS3)
            self.assertIsNone(event)

    @patch('banhost.network.gethostname', return_value=True)
    def test_insert_event_error(self, mock_gethostname):
        """Integrity error on event insertion."""
        host = banhost.network.Host(TEST_IPV6_ADDRESS3, 6)
        event = banhost.network.Event.make(host, duration=10)
        with contextlib.closing(Database(self.dbname)) as db:
            with db.connection.transaction() as tr:
                tr.execute(('INSERT INTO hosts VALUES (NULL, ?, ?, ?)', (host.address, host.ipversion, '')))
            # Trick the db to think the host hasn't been inserted yet
            with patch.object(Transaction, 'fetch_one') as mock_fetch_one:
                mock_fetch_one.return_value = None
                with self.assertRaises(DatabaseError):
                    db.insert_new_event(event)

    @patch('banhost.network.gethostname', return_value=True)
    def test_events_same_host(self, mock_gethostname):
        """Add multiples events with same host."""
        self.assertFalse(os.path.exists(self.dbname))
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        event1 = banhost.network.Event.make(host1, duration=10)
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        event2 = banhost.network.Event.make(host2, event1.end_time, duration=10)

        # Add event
        with contextlib.closing(Database(self.dbname)) as db:
            self.assertEqual(0, len(db.collect_current_events()))
            self.assertIsNone(event1.host.name)
            db.insert_new_event(event1)
            self.assertIsNotNone(event1.host.name)

        with contextlib.closing(Database(self.dbname)) as db:
            # Check content
            self.assertEqual([ event1 ], db.collect_current_events())

            # No duplicates
            inserted_event = db.insert_new_event(event2)
            self.assertNotEqual(inserted_event, event2)

            # Delete event
            db.delete_current_event(event1.host.address)
            self.assertEqual(0, len(db.collect_current_events()))
            with self.assertRaises(DatabaseError):
                db.delete_current_event(event1.host.address)

            # Insert new event
            new_event2 = db.insert_new_event(event2)
            self.assertEqual([ event2 ], db.collect_current_events())
            mock_gethostname.assert_called_once_with(TEST_IPV6_ADDRESS1)
            self.assertIsNone(event2.host.name)
            self.assertIsNotNone(new_event2.host.name)

            # Clear banhost
            db.clear_current_events()
            self.assertEqual(0, len(db.collect_current_events()))

    @patch('banhost.network.gethostname', return_value=True)
    def test_expired_events(self, mock_gethostname):
        """Test expired banhost events."""
        self.assertFalse(os.path.exists(self.dbname))
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host2 = banhost.network.Host(TEST_IPV4_ADDRESS1, 4)
        valid_event = banhost.network.Event.make(host1, duration=300)
        expired_event = banhost.network.Event.make(host2, valid_event.start_time - 300, duration=10)

        with contextlib.closing(Database(self.dbname)) as db:
            db.insert_new_event(expired_event)
            db.insert_new_event(valid_event)
            self.assertEqual([ expired_event, valid_event ], db.collect_current_events())

        with contextlib.closing(Database(self.dbname)) as db:
            expiration_time = (expired_event.end_time + valid_event.start_time)/2
            expired_events = db.delete_expired_events(expiration_time)
            self.assertEqual([ expired_event ], expired_events)

    @patch('banhost.network.gethostname', side_effect=fake_gethostname)
    def test_past_events(self, mock_gethostname):
        """Insertion, deletion of past events."""
        self.assertFalse(os.path.exists(self.dbname))
        now = banhost.network.Event.now()
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        event1 = banhost.network.Event.make(host1, start_time=now - 300, duration=10)
        event2 = banhost.network.Event.make(host2, event1.end_time + 5, duration=10)

        # Add events
        with contextlib.closing(Database(self.dbname)) as db:
            past_events = [ event for event in db.iterate_past_events() ]
            self.assertEqual(0, len(past_events))
            db.insert_new_event(event1)
            db.insert_new_event(event2)

        # Move events to past events
        with contextlib.closing(Database(self.dbname)) as db:
            db.delete_expired_events(now, keep=True)
            current_events = db.collect_current_events()
            self.assertEqual(0, len(current_events))

        # Delete events
        with contextlib.closing(Database(self.dbname)) as db:
            past_events = sorted([ event for event in db.iterate_past_events() ])
            self.assertEqual([ event1, event2 ], past_events)
            db.delete_past_events_older_than((event1.start_time + event1.end_time)/2)
            past_events = db.collect_past_events()
            self.assertEqual([ event2 ], past_events)

    @patch('banhost.network.gethostname', side_effect=fake_gethostname)
    def test_count_past_events(self, mock_gethostname):
        """Count past events."""
        self.assertFalse(os.path.exists(self.dbname))
        now = banhost.network.Event.now()
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host1_again = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        batches = [
            [ banhost.network.Event.make(host1, start_time=now - 300, duration=10),
              banhost.network.Event.make(host2, start_time=now - 200, duration=10) ],
            [ banhost.network.Event.make(host1_again, start_time=now - 100, duration=10) ]
        ]

        # Add events
        with contextlib.closing(Database(self.dbname)) as db:
            past_events = [ event for event in db.iterate_past_events() ]
            self.assertEqual(0, len(past_events))
            for events in batches:
                for event in events:
                    db.insert_new_event(event)
                db.delete_expired_events(now, keep=True)

        # Check counts
        with contextlib.closing(Database(self.dbname)) as db:
            self.assertEqual(2, db.count_past_events_for(host1.address))
            self.assertEqual(1, db.count_past_events_for(host2.address))
            self.assertEqual(0, db.count_past_events_for(TEST_IPV6_ADDRESS3))

    @patch('banhost.network.gethostname', side_effect=fake_gethostname)
    def test_delete_unused_hosts(self, mock_gethostname):
        """Delete hosts that aren't referenced anymore"""
        self.assertFalse(os.path.exists(self.dbname))
        now = banhost.network.Event.now()
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        host3 = banhost.network.Host(TEST_IPV6_ADDRESS3, 6)
        event1 = banhost.network.Event.make(host1, start_time=now - 300, duration=10)
        event2 = banhost.network.Event.make(host2, event1.end_time + 20, duration=10)
        event3 = banhost.network.Event.make(host3, event2.end_time + 20, duration=10)

        with contextlib.closing(Database(self.dbname)) as db:
            # Add events
            with patch.object(banhost.network.Host, 'resolve', return_value=''):
                for event in [ event1, event2, event3 ]:
                    db.insert_new_event(event)
            # Move events to history
            db.delete_expired_events(now, keep=True)
            # Delete all events but 1
            db.delete_past_events_older_than(event3.start_time - 5)

            with db.connection.transaction() as transaction:
                # Check hosts
                expected_hosts = set([ host1.address, host2.address, host3.address ])
                current_hosts = set([ row[0] for row in transaction.execute(('SELECT address FROM hosts',)) ])
                self.assertEqual(expected_hosts, current_hosts)
                # Check events
                events_count = transaction.fetch_single(('SELECT COUNT(*) FROM history',))
                self.assertEqual(1, events_count)

        # Delete hosts
        with contextlib.closing(Database(self.dbname)) as db:
            db.delete_hosts_without_events()

            with db.connection.transaction() as transaction:
                expected_hosts = set([ host3.address ])
                current_hosts = set([ row[0] for row in transaction.execute(('SELECT address FROM hosts',)) ])
                self.assertEqual(expected_hosts, current_hosts)

    @patch('banhost.network.gethostname', side_effect=fake_gethostname)
    def test_vacuum(self, mock_gethostname):
        """Compact database after erasing content."""
        self.assertFalse(os.path.exists(self.dbname))
        now = banhost.network.Event.now()

        # Add many events
        with contextlib.closing(Database(self.dbname)) as db:
            for host_number in range(1, 256):
                host = banhost.network.Host('10.0.0.{}'.format(host_number), 4)
                event = banhost.network.Event.make(host, now - host_number * 100, duration=host_number)
                db.insert_new_event(event)
        initial_size = os.path.getsize(self.dbname)
        self.assertTrue(initial_size > 0)

        # Delete them
        with contextlib.closing(Database(self.dbname)) as db:
            db.delete_expired_events(now, keep=False)
        before_vacuum_size = os.path.getsize(self.dbname)
        self.assertEqual(initial_size, before_vacuum_size)

        # Vacuum
        with contextlib.closing(Database(self.dbname)) as db:
            db.vacuum()
        after_vacuum_size = os.path.getsize(self.dbname)
        self.assertTrue(after_vacuum_size < before_vacuum_size)


class TestDatabaseUpgrade(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_banhost')
        self.dbname = os.path.join(self.working_dir, 'banhost.db')

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def fetch_all(self, connection, sql):
        with connection.transaction() as transaction:
            return transaction.execute((sql,)).fetchall()

    def fetch_v4_events(self, connection, table):
        sql = "SELECT address, ipversion, start_time, end_time FROM {} ORDER BY address".format(table.name)
        return self.fetch_all(connection, sql)

    def fetch_v5_events(self, connection, table):
        sql = ("SELECT h.address, h.ipversion, e.start_time, e.end_time FROM" +
               " hosts AS h, {} AS e WHERE e.host = h.id ORDER BY h.address".format(table.name))
        return self.fetch_all(connection, sql)

    def fetch_v5_hosts(self, connection, table):
        sql = "SELECT address, ipversion, name FROM hosts ORDER BY address"
        return self.fetch_all(connection, sql)

    @patch('socket.gethostbyaddr', side_effect=fake_gethostbyaddr)
    def test_upgrade_v4_to_v5(self, mock_gethostbyaddr):
        self.assertFalse(DatabaseUpgrade(self.dbname).run(), msg="upgrade must return False when database not found")

        # create database content
        with Connection(self.dbname) as connection:
            with connection.transaction() as transaction:
                transaction.cursor.executescript(CONTENT_V4)
            expected_banhost = self.fetch_v4_events(connection, Database.TABLES.blocklist)
            expected_history = self.fetch_v4_events(connection, Database.TABLES.history)
        self.assertEqual(2, len(expected_banhost))
        self.assertEqual(4, len(expected_history))

        upgrader = DatabaseUpgrade(self.dbname)
        self.assertTrue(upgrader.run())
        with Connection(self.dbname) as connection:
            # check table banhost
            banhost_info = table_info2tuple(connection, Database.TABLES.blocklist)
            self.assertIsNotNone(banhost_info)
            self.assertEqual(banhost.sqlite.EventTable.COLUMNS, banhost_info)
            current_banhost = self.fetch_v5_events(connection, Database.TABLES.blocklist)
            self.assertEqual(expected_banhost, current_banhost)
            # check table history
            history_info = table_info2tuple(connection, Database.TABLES.history)
            self.assertIsNotNone(history_info)
            self.assertEqual(banhost.sqlite.EventTable.COLUMNS, history_info)
            current_history = self.fetch_v5_events(connection, Database.TABLES.history)
            self.assertEqual(expected_history, current_history)
            # check table hosts
            host_info = table_info2tuple(connection, Database.TABLES.hosts)
            self.assertIsNotNone(host_info)
            self.assertEqual(banhost.sqlite.HostTable.COLUMNS, host_info)
            expected_hosts = [('198.51.100.69', 4, 'host1.somewhere.net'), ('fc00:4f8:0:2::70', 6, 'host2.elsewhere.net')]
            hosts = [ row for row in self.fetch_v5_hosts(connection, Database.TABLES.hosts) if row[2] ]
            self.assertEqual(expected_hosts, hosts)

        # no double upgrade
        self.assertFalse(upgrader.run())

if __name__ == '__main__':
    unittest.main()
