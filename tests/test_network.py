# Copyright (C) 2005-2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import banhost.network
import re
import socket
import unittest
import unittest.mock

from tests.helpers.asserts import *
from tests.helpers.dns import *

class TestFunctions(unittest.TestCase):

    """Test static functions."""

    @unittest.mock.patch('socket.gethostbyaddr', side_effect=fake_gethostbyaddr)
    def test_gethostname(self, mock_gethostbyaddr):
        """Test function gethostname"""
        self.assertEqual(TEST_HOST_NAME1, banhost.network.gethostname(TEST_IPV4_ADDRESS1))
        self.assertEqual(TEST_HOST_NAME1, banhost.network.gethostname(TEST_IPV6_ADDRESS1))
        self.assertIsNone(banhost.network.gethostname(TEST_IPV6_ADDRESS3))
        assert_called(mock_gethostbyaddr)

    def test_format_duration(self):
        """Test function test_format_duration"""
        samples = [(0, ''), (1, '1 second'), (15, '15 seconds'), (59, '59 seconds'),
                   (60, '1 minute'), (61, '1 minute, 1 second'), (301, '5 minutes, 1 second'), (3599, '59 minutes, 59 seconds'),
                  (3600, '1 hour'), (7215, '2 hours, 15 seconds'), (39706, '11 hours, 1 minute, 46 seconds') ]
        for duration, result in samples:
            self.assertEqual(banhost.network.format_duration(duration), result)

class TestHost(unittest.TestCase):

    """Test class Host."""

    def check_host(self, host):
        """Check host attributes."""
        self.assertIn(host.ipversion, [4, 6])
        self.assertEqual(TEST_HOST_NAME1, host.name)
        if host.ipversion == 4:
            self.assertEqual(TEST_IPV4_ADDRESS1, host.address)
            self.assertEqual('{} ({})'.format(TEST_IPV4_ADDRESS1, TEST_HOST_NAME1), str(host))
        elif host.ipversion == 6:
            self.assertEqual(TEST_IPV6_ADDRESS1, host.address)
            self.assertEqual('{} ({})'.format(TEST_IPV6_ADDRESS1, TEST_HOST_NAME1), str(host))

    def test_operators(self):
        """Operators on hosts"""
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host1_again = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        host3 = banhost.network.Host(TEST_IPV4_ADDRESS1, 4)
        self.assertEqual(host1, host1)
        self.assertEqual(host1, host1_again)
        self.assertNotEqual(host1, host2)
        self.assertNotEqual(host1, 'string')
        self.assertEqual("('{}', 6)".format(TEST_IPV6_ADDRESS1), repr(host1))
        self.assertTrue(host3 < host1)

    @unittest.mock.patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @unittest.mock.patch('socket.gethostbyaddr', side_effect=fake_gethostbyaddr)
    def test_ipv4(self, mock_gethostbyaddr, mock_getaddrinfo):
        """Test host known by its IPv4 address."""
        hosts = banhost.network.Host.resolve_hosts(TEST_IPV4_ADDRESS1)
        self.assertEqual(1, len(hosts))
        for host in hosts:
            self.check_host(host)
        assert_called_once(mock_gethostbyaddr)
        assert_not_called(mock_getaddrinfo)

    @unittest.mock.patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @unittest.mock.patch('socket.gethostbyaddr', side_effect=fake_gethostbyaddr)
    def test_ipv6(self, mock_gethostbyaddr, mock_getaddrinfo):
        """Test host known by its IPv6 address."""
        hosts = banhost.network.Host.resolve_hosts(TEST_IPV6_ADDRESS1)
        self.assertEqual(1, len(hosts))
        for host in hosts:
            self.check_host(host)
        first_host = hosts[0]
        self.assertEqual({ 'address': TEST_IPV6_ADDRESS1, 'name': TEST_HOST_NAME1 }, first_host._asdict())
        assert_called_once(mock_gethostbyaddr)
        assert_not_called(mock_getaddrinfo)

    @unittest.mock.patch('socket.gethostbyaddr', side_effect=fake_gethostbyaddr)
    @unittest.mock.patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    def test_name(self, mock_getaddrinfo, mock_gethostbyaddr):
        """Test host known by its name."""
        hosts = banhost.network.Host.resolve_hosts(TEST_HOST_NAME1)
        self.assertEqual(2, len(hosts))
        for host in hosts:
            self.check_host(host)
        assert_called_once(mock_getaddrinfo)
        assert_not_called(mock_gethostbyaddr)

    @unittest.mock.patch('socket.gethostbyaddr', side_effect=fake_gethostbyaddr)
    @unittest.mock.patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    def test_unknown(self, mock_getaddrinfo, mock_gethostbyaddr):
        """Test unknown host."""
        hosts = banhost.network.Host.resolve_hosts(TEST_IPV6_ADDRESS3)
        self.assertEqual(1, len(hosts))
        host = hosts.pop(0)
        self.assertEqual('', host.name)
        self.assertEqual({ 'address': TEST_IPV6_ADDRESS3 }, host._asdict())
        self.assertEqual(TEST_IPV6_ADDRESS3, str(host))
        assert_not_called(mock_getaddrinfo)
        assert_called(mock_gethostbyaddr)

    @unittest.mock.patch('socket.gethostname', return_value='localhost')
    def test_myself(self, mock_gethostname):
        """Test this host."""
        hosts = banhost.network.Host.resolve_myself()
        self.assertTrue(len(hosts) > 0)
        assert_called_once(mock_gethostname)

    def test_host_as_dict(self):
        """Host as dictionary."""
        # without name
        host1 = banhost.network.Host(TEST_IPV4_ADDRESS1, 4)
        self.assertEqual({'address': TEST_IPV4_ADDRESS1}, host1._asdict())
        # with name
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6, TEST_HOST_NAME1)
        expected_host2 = {'address': TEST_IPV6_ADDRESS1, 'name': TEST_HOST_NAME1}
        self.assertEqual(expected_host2, host2._asdict())
        # extended with extra attributes
        host3 = host2.extend(valid=True, direction=host2.DIRECTION_INCOMING)
        expected_host3 = {'valid': True, 'direction': host2.DIRECTION_INCOMING}
        expected_host3.update(expected_host2)
        self.assertEqual(expected_host3, host3._asdict())


class TestEvent(unittest.TestCase):

    """Test class Event."""

    def test_operators(self):
        """Operators on events"""
        start_time = banhost.network.Event.now()
        duration = 10
        # Equality
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        event1 = banhost.network.Event.make(host1, start_time, duration)
        host1_again = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        event1_again = banhost.network.Event.make(host1_again, start_time, duration)
        self.assertEqual(event1, event1_again)
        # Non equality
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        event2 = banhost.network.Event.make(host2, start_time, duration)
        self.assertNotEqual(event1, event2)
        self.assertNotEqual(event1, 'string')
        # Less than with start time
        host3 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        event3 = banhost.network.Event.make(host3, start_time + 1, duration)
        self.assertTrue(event2 < event3)
        self.assertFalse(event3 < event2)
        # Less than with end time
        host4 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        event4 = banhost.network.Event.make(host4, start_time, duration + 1)
        self.assertTrue(event2 < event4)
        self.assertFalse(event4 < event2)

    def test_stringRepresentation(self):
        """Test repr and str"""
        host = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        event = banhost.network.Event.make(host, 1546169285, 10)
        self.assertEqual("(('fc00:4f8:0:2::70', 6), 1546169285, 1546169295)", repr(event))
        str_re = re.compile(r'fc00:4f8:0:2::70 from \d{4}(?:/\d{2}){2} \d{2}(?::\d{2}){2} to \d{4}(?:/\d{2}){2} \d{2}(?::\d{2}){2}$')
        self.assertTrue(str_re.match(str(event)))

    def test_localTime(self):
        """Test local time"""
        host = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        event = banhost.network.Event.make(host, 1546123012, 70)
        time_re = re.compile(r'\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}$')
        start_time = event.local_start_time()
        end_time = event.local_end_time()
        self.assertIsNotNone(time_re.match(start_time))
        self.assertIsNotNone(time_re.match(end_time))
        self.assertNotEqual(start_time, end_time)

        non_event = banhost.network.Event(host, None, None)
        self.assertEqual('<undefined>', non_event.local_start_time())
        self.assertEqual('<undefined>', non_event.local_end_time())

    def test_makeEvent(self):
        """Test creation of an event."""
        host = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        start_time = banhost.network.Event.now()
        duration = 10
        event = banhost.network.Event.make(host, start_time, duration)
        with self.assertRaises(Exception):
            banhost.network.Event.make(host, start_time, 0)
        self.assertEqual(host.address, event.host.address)
        self.assertEqual(start_time, event.start_time)
        self.assertEqual(start_time + duration, event.end_time)
        self.assertEqual(duration, event.duration())


class TestTimeLines(unittest.TestCase):

    """Test class TimeLines."""

    def test_singleTimeLine(self):
        """Test a single timeline."""
        timelines = banhost.network.TimeLines()
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        start_time = banhost.network.Event.now()
        duration = 10
        event1 = banhost.network.Event.make(host1, start_time, duration)
        timelines.add_event(event1)
        event2 = banhost.network.Event.make(host2, event1.end_time + duration, duration)
        timelines.add_event(event2)
        result = timelines.finalize()
        self.assertEqual(1, len(result))
        if result:
            timeline = result[0]
            self.assertEqual([ event1, event2 ], timeline)

    def test_generate(self):
        """Generate a timeline."""
        host1 = banhost.network.Host(TEST_IPV6_ADDRESS1, 6)
        host2 = banhost.network.Host(TEST_IPV6_ADDRESS2, 6)
        start_time = banhost.network.Event.now()
        duration = 10
        event1 = banhost.network.Event.make(host1, start_time, duration)
        event2 = banhost.network.Event.make(host2, event1.end_time + duration, duration)
        result = banhost.network.TimeLines.generate([ event1, event2 ])
        self.assertEqual(2, len(result))
        self.assertEqual([[ event1 ], [ event2 ]], result)

if __name__ == '__main__':
    unittest.main()
