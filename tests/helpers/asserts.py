# Copyright (C) 2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

__all__ = [ 'assert_called', 'assert_called_once', 'assert_not_called' ]

def assert_called(mock):
    try:
        mock.assert_called()
    except AttributeError:
        # python < 3.6
        if mock.call_count == 0:
            raise AssertionError('Expected {} to be called'.format(mock.__name__))

def assert_called_once(mock):
    try:
        mock.assert_called_once()
    except AttributeError:
        # python < 3.6
        if mock.call_count != 1:
            raise AssertionError('Expected {} to be called once instead of {} times'.format(mock.__name__, mock.call_count))

def assert_not_called(mock):
    try:
        mock.assert_not_called()
    except AttributeError:
        # python < 3.6
        if mock.call_count != 0:
            raise AssertionError('Expected {} not to be called'.format(mock.__name__))

