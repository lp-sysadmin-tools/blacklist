# Copyright (C) 2019 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

__all__ = [ 'CONTENT_V4' ]

CONTENT_V4 = """PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE blocklist (address text primary key, ipversion integer, start_time integer, end_time integer);
INSERT INTO "blocklist" VALUES('198.51.100.69',4,1546208272,1546258672);
INSERT INTO "blocklist" VALUES('10.0.11.92',4,1546226087,1546262087);
CREATE TABLE history (address text, ipversion integer, start_time integer, end_time integer);
INSERT INTO "history" VALUES('198.51.100.167',4,1545655476,1545662676);
INSERT INTO "history" VALUES('10.0.11.92',4,1545655510,1545662710);
INSERT INTO "history" VALUES('fc00:4f8:0:2::70',6,1545655690,1545662890);
INSERT INTO "history" VALUES('192.168.2.206',4,1545656261,1545663461);
COMMIT;
"""
