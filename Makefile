GITIGNORE = .gitignore __pycache_ banhost.egg-info build dist

SOURCES = $(wildcard banhost/*.py tests/*.py tests/*/*.py)

COVERAGE_RUN = env PYTHONPATH=$(PWD) coverage run --source=banhost
COVERAGE_REPORT = coverage report -m

SED_EXPR_VERSION = "s/^__version__ = '\\(.*\\)'/\\1/"

PYTHON ?= python

PYTHON_RUN = env PYTHONPATH=$(PWD) $(PYTHON)

build-dist:
	$(PYTHON) setup.py bdist

test:
	$(PYTHON_RUN) -m unittest discover

coverage:
	$(COVERAGE_RUN) -m unittest discover
	$(COVERAGE_REPORT)

test-%:
	$(PYTHON_RUN) tests/test_$*.py

coverage-%:
	$(COVERAGE_RUN) tests/test_$*.py
	$(COVERAGE_REPORT)

debug-%:
	$(PYTHON_RUN) -m pdb tests/test_$*.py

.gitignore:
	@for NAME in $(GITIGNORE); do echo $$NAME; done > $@ 

clean: .gitignore
	-rm -rf banhost.egg-info build dist
	-rm -rf */__pycache__ */*/__pycache__

version:
	@sed $(SED_EXPR_VERSION) banhost/version.py

release:
	@git diff --quiet || { echo "Error: there are uncommited changes"; exit 1; }
	VERSION=$$(sed $(SED_EXPR_VERSION) banhost/version.py) &&	\
		git tag -a -m "Release $$VERSION" r$$VERSION &&				\
		git push --follow-tags

TAGS: $(SOURCES)
	etags --language=python --output=$@ $+
